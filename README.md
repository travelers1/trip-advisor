# Jakarta City Tour
Sebuah program yang membantu user untuk menemukan rute dan jadwal perjalanan wisata di kota Jakarta berdasarkan
waktu lamanya berwisata serta jam mulainya wisata.

## Spesifikasi program
Pada program disediakan beberapa fakta seperti fakta mengenai tempat wisata serta estimasi waktu berwisata,
lama perjalanan dari satu tempat wisata ke tempat wisata yang lain, serta fakta jam kunjung dari setiap tempat wisata.
Program kami memberi toleransi waktu maksimal 15 menit terhadap keinginan lamanya user berwisata. Misalnya apabila user menginginkan
berwisata selama 1 jam, maka program kami memungkinkan terdapat hasil yang lama berwisatanya minimal hanya 45 menit. Kami merancang demikian
karena pada dunia nyata sulit sekali merancang waktu perjalanan yang benar-benar tepat waktu (biasanya saat berwisata waktu yang diberikan
berupa estimasi). Oleh karena itu, agar program kami dapat lebih selaras dengan dunia nyata maka kami merancang waktu pada program ini 
berdasarkan estimasi.

## Eksekusi Program
1.  User memasukan query jalan_jalan
2.  User diminta memasukkan berapa lama perjalanan serta waktu mulainya perjalanan yang diinginkan
3.  Program akan menampilkan total kemungkinan rute serta seluruh kemungkinan rute.

Berikut adalah beberapa contoh eksekusi program:
1. User ingin berjalan-jalan selama 50 menit dan memulai perjalanan pukul 9 pagi.
```
?- jalan_jalan.
Berapa lama anda ingin berwisata dalam 1 hari?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: 00:50.

Jam berapa anda mulai ingin berwisata?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: |: 09:00.

Banyak solusi yang ditemukan: 2

Berikut adalah kemungkinan rute perjalanan:
1. [mulai_pukul(9:0),tiba(Museum Fatahillah),pukul(9:0),berwisata(Museum Fatahillah,0:50),selesai_pukul(9:50)]
2. [mulai_pukul(9:0),tiba(Museum Perangko),pukul(9:0),berwisata(Museum Perangko,0:45),selesai_pukul(9:45)]
true .
```

2. User ingin berjalan-jalan selama 1 jam dan perjalanan dimulai pukul 6 pagi. Terlihat bahwa tidak ada hasil yang ditemukan karena berdasarkan fakta yang ada tidak ada tempat wisata yang buka pada pukul 6.
```
?- jalan_jalan.
Berapa lama anda ingin berwisata dalam 1 hari?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: 01:00.

Jam berapa anda mulai ingin berwisata?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: |: 06:00.

Tidak ada rute yang dapat ditempuh dengan lama perjalanan 1:0 dan waktu mulai pukul 6:0.
Mohon cek jadwal buka setiap tempat wisata dengan mengetikkan perintah 'jam_operasional.'
true.
```

User dapat mengecek jadwal buka dari setiap tempat wisata dengan memberi query 'jam_operasional.'
```
?- jam_operasional.
Berikut adalah jam operasional setiap tempat wisata di Jakarta:
Ancol:               07:00 - 24:00
Ragunan:             08:00 - 16:00
Dufan:               10:00 - 18:00
Taman Mini:          07:00 - 22:00
Museum Macan:        10:00 - 18:00
Kota Tua:            07:00 - 24:00
Planetarium:         08:00 - 15:00
Monas:               07:00 - 24:00
Museum Perangko:     07:00 - 22:00
Museum Fatahillah:   09:00 - 17:00
true.
```

3. User ingin berjalan-jalan selama 3 jam namun perjalanan dimulai pukul 08:30 pagi.
```
?- jalan_jalan.
Berapa lama anda ingin berwisata dalam 1 hari?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: 03:00.

Jam berapa anda mulai ingin berwisata?
(jawab dengan format JJ:MM dan akhiri dengan tanda titik)
Jawab: |: 08:30.

Banyak solusi yang ditemukan: 3

Berikut adalah kemungkinan rute perjalanan:
1. [mulai_pukul(8:30),tiba(Kota Tua),pukul(8:30),berwisata(Kota Tua,2:45),selesai_pukul(11:15)]
2. [mulai_pukul(8:30),tiba(Monas),pukul(8:30),berwisata(Monas,1:20),perjalanan(Monas,Museum Fatahillah,0:45),tiba(Museum Fatahillah),pukul(10:35),berwisata(Museum Fatahillah,0:50),selesai_pukul(11:25)]
3. [mulai_pukul(8:30),tiba(Museum Perangko),pukul(8:30),berwisata(Museum Perangko,0:45),perjalanan(Museum Perangko,Monas,0:45),tiba(Monas),pukul(10:0),berwisata(Monas,1:20),selesai_pukul(11:20)]
true.
```

