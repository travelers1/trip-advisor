/*Fakta mengenai tempat wisata serta estimasi waktu yang
dihabiskan dalam setiap tempat wisata */
wisata('Ancol',05:00).
wisata('Ragunan', 05:00).
wisata('Dufan', 05:00).
wisata('Taman Mini',02:30).
wisata('Museum Macan', 02:30).
wisata('Kota Tua', 02:45).
wisata('Planetarium', 01:45).
wisata('Monas', 01:20).
wisata('Museum Perangko', 00:45).
wisata('Museum Fatahillah', 00:50).

% Fakta waktu tempuh dari satu tempat ke tempat lain
% Dari Ancol
perjalanan('Ancol', 'Taman Mini', 02:15).
perjalanan('Ancol', 'Ragunan', 01:12).
perjalanan('Ancol', 'Dufan', 00:15).
perjalanan('Ancol', 'Kota Tua', 00:25).
perjalanan('Ancol', 'Museum Macan', 01:15).
perjalanan('Ancol', 'Planetarium', 01:45).
perjalanan('Ancol', 'Monas', 01:20).
perjalanan('Ancol', 'Museum Perangko', 01:00).
perjalanan('Ancol', 'Museum Fatahillah', 01:30).

% Dari Ragunan
perjalanan('Ragunan', 'Ancol', 01:12).
perjalanan('Ragunan', 'Dufan', 01:10).
perjalanan('Ragunan', 'Taman Mini', 02:00).
perjalanan('Ragunan', 'Monas', 01:15).
perjalanan('Ragunan', 'Museum Macan', 01:20).
perjalanan('Ragunan', 'Kota Tua', 02:23).
perjalanan('Ragunan', 'Planetarium', 01:30).
perjalanan('Ragunan', 'Meseum Perangko', 01:35).
perjalanan('Ragunan', 'Museum Fatahillah', 02:40).

% Dari Dufan
perjalanan('Dufan', 'Ancol', 00:15).
perjalanan('Dufan', 'Ragunan', 01:10).
perjalanan('Dufan', 'Taman Mini', 01:00).
perjalanan('Dufan', 'Monas', 01:30).
perjalanan('Dufan', 'Museum Macan', 02:00).
perjalanan('Dufan', 'Kota Tua', 02:00).
perjalanan('Dufan', 'Planetarium', 00:45).
perjalanan('Dufan', 'Meseum Perangko', 02:00).
perjalanan('Dufan', 'Museum Fatahillah', 01:00).

% Dari Taman Mini
perjalanan('Taman Mini', 'Ancol', 02:15).
perjalanan('Taman Mini', 'Ragunan', 02:00).
perjalanan('Taman Mini', 'Dufan', 01:00).
perjalanan('Taman Mini', 'Museum Macan', 01:00).
perjalanan('Taman Mini', 'Kota Tua', 02:00).
perjalanan('Taman Mini', 'Planetarium', 00:50).
perjalanan('Taman Mini', 'Monas', 01:15).
perjalanan('Taman Mini', 'Museum Perangko', 01:20).
perjalanan('Taman Mini', 'Museum Fatahillah', 01:00).

% Dari Museum Macan
perjalanan('Museum Macan', 'Ancol', 01:15).
perjalanan('Museum Macan', 'Ragunan', 01:20).
perjalanan('Museum Macan', 'Dufan', 02:00).
perjalanan('Museum Macan', 'Taman Mini', 01:00).
perjalanan('Museum Macan', 'Kota Tua', 02:00).
perjalanan('Museum Macan', 'Planetarium', 01:30).
perjalanan('Museum Macan', 'Monas', 01:45).
perjalanan('Museum Macan', 'Museum Perangko', 02:00).
perjalanan('Museum Macan', 'Museum Fatahillah', 02:00).

% Dari Kota Tua
perjalanan('Kota Tua', 'Ancol', 00:25).
perjalanan('Kota Tua', 'Ragunan', 02:23).
perjalanan('Kota Tua', 'Dufan', 02:00).
perjalanan('Kota Tua', 'Taman Mini', 02:00).
perjalanan('Kota Tua', 'Museum Macan', 02:00).
perjalanan('Kota Tua', 'Planetarium', 02:00).
perjalanan('Kota Tua', 'Monas', 01:55).
perjalanan('Kota Tua', 'Museum Perangko', 01:00).
perjalanan('Kota Tua', 'Museum Fatahillah', 01:00).

% Dari Planetarium
perjalanan('Planetarium', 'Ancol', 01:45).
perjalanan('Planetarium', 'Ragunan', 01:30).
perjalanan('Planetarium', 'Dufan', 00:45).
perjalanan('Planetarium', 'Taman Mini', 00:50).
perjalanan('Planetarium', 'Museum Macan', 01:30).
perjalanan('Planetarium', 'Kota Tua', 02:00).
perjalanan('Planetarium', 'Monas', 00:45).
perjalanan('Planetarium', 'Museum Perangko', 00:50).
perjalanan('Planetarium', 'Museum Fatahillah', 01:15).

% Dari Monas
perjalanan('Monas', 'Ancol', 01:20).
perjalanan('Monas', 'Ragunan', 01:15).
perjalanan('Monas', 'Dufan', 01:30).
perjalanan('Monas', 'Taman Mini', 01:15).
perjalanan('Monas', 'Museum Macan', 01:45).
perjalanan('Monas', 'Kota Tua', 01:55).
perjalanan('Monas', 'Planetarium', 00:45).
perjalanan('Monas', 'Museum Perangko', 01:20).
perjalanan('Monas', 'Museum Fatahillah', 00:45).

% Dari Museum Perangko
perjalanan('Museum Perangko', 'Ancol', 01:00).
perjalanan('Museum Perangko', 'Ragunan', 01:35).
perjalanan('Museum Perangko', 'Dufan', 01:30).
perjalanan('Museum Perangko', 'Taman Mini', 01:15).
perjalanan('Museum Perangko', 'Museum Macan', 01:45).
perjalanan('Museum Perangko', 'Kota Tua', 01:55).
perjalanan('Museum Perangko', 'Planetarium', 00:45).
perjalanan('Museum Perangko', 'Monas', 00:45).
perjalanan('Museum Perangko', 'Museum Fatahillah', 01:00).

% Dari Museum Fatahillah
perjalanan('Museum Fatahillah', 'Ancol', 01:30).
perjalanan('Museum Fatahillah', 'Ragunan', 02:40).
perjalanan('Museum Fatahillah', 'Dufan', 01:00).
perjalanan('Museum Fatahillah', 'Taman Mini', 01:00).
perjalanan('Museum Fatahillah', 'Museum Macan', 02:00).
perjalanan('Museum Fatahillah', 'Kota Tua', 01:00).
perjalanan('Museum Fatahillah', 'Planetarium', 01:15).
perjalanan('Museum Fatahillah', 'Monas', 00:45).
perjalanan('Museum Fatahillah', 'Museum Perangko', 01:00).

% Fakta mengenai jam buka setiap tempat wisata
jam_buka('Ancol', 07:00, 24:00).
jam_buka('Ragunan', 08:00, 16:00).
jam_buka('Dufan', 10:00, 18:00).
jam_buka('Taman Mini', 07:00, 22:00).
jam_buka('Museum Macan', 10:00, 18:00).
jam_buka('Kota Tua', 07:00, 24:00).
jam_buka('Planetarium', 08:00, 15:00).
jam_buka('Monas', 07:00, 24:00).
jam_buka('Museum Perangko', 07:00, 22:00).
jam_buka('Museum Fatahillah', 09:00, 17:00).


% Entry Point program
jalan_jalan :- 
    write('Berapa lama anda ingin berwisata dalam 1 hari?'),
    nl,
    write('(jawab dengan format JJ:MM dan akhiri dengan tanda titik)'),
    nl,
    write('Jawab: '),
    read(Lama),
    nl,
    write('Jam berapa anda mulai ingin berwisata?'),
    nl,
    write('(jawab dengan format JJ:MM dan akhiri dengan tanda titik)'),
    nl,
    write('Jawab: '),
    read(Mulai),
    nl,
    solution(_, Lama, Mulai).

% Rule untuk mengumpulkan solusi kemudian menuliskannya ke stream
solution(Wisata, Lama, Mulai):- 
    setof(Wisata, jadwal(Wisata,Lama,Mulai), List0fSolution),
    write('Banyak solusi yang ditemukan: '),
    length(List0fSolution, L),
    write(L),
    nl,
    nl,
    write('Berikut adalah kemungkinan rute perjalanan:'),
    nl,
    display(1, List0fSolution), 
    !.

solution(_, Lama, Mulai):- 
    write('Tidak ada rute yang dapat ditempuh dengan lama perjalanan '),
    write(Lama),
    write(' dan waktu mulai pukul '),
    write(Mulai),
    write('.'),
    nl,
    write('Mohon cek jadwal buka setiap tempat wisata dengan mengetikkan perintah \'jam_operasional.\'').

/*Rule khusus untuk mengiterasikan solusi yang telah dikumpulkan agar
dapat ditampilkan dengan lebih rapi*/
display(_,[]).
display(Num, [H|T]) :- 
    write(Num), 
    write('. '), 
    write(H),
    NewNum is Num+1,
    nl,
    display(NewNum, T).

% Rule yang menampilkan jam operasional setiap tempat wisata
jam_operasional :-
    write('Berikut adalah jam operasional setiap tempat wisata di Jakarta:'),
    nl,
    write('Ancol:'), tab(15), write('07:00 - 24:00'),
    nl,
    write('Ragunan:'), tab(13), write('08:00 - 16:00'),
    nl,
    write('Dufan:'), tab(15), write('10:00 - 18:00'),
    nl,
    write('Taman Mini:'), tab(10), write('07:00 - 22:00'),
    nl,
    write('Museum Macan:'), tab(8), write('10:00 - 18:00'),
    nl,
    write('Kota Tua:'), tab(12), write('07:00 - 24:00'),
    nl,
    write('Planetarium:'), tab(9), write('08:00 - 15:00'),
    nl,
    write('Monas:'), tab(15), write('07:00 - 24:00'),
    nl,
    write('Museum Perangko:'), tab(5), write('07:00 - 22:00'),
    nl,
    write('Museum Fatahillah:'), tab(3), write('09:00 - 17:00').
    

% Rule yang digunakan untuk mengecek apakah jam yang diinput sudah benar
is_jam(24:00) :- !.
is_jam(Jam:Menit) :- 
    Jam =< 23, Jam >= 0, 
    Menit =< 59, 
    Menit >= 0. 

% Rule untuk mengeluarkan hasil penambahan jam
add_jam(J1:M1, J2:M2, JA:MA) :- 
    is_jam(J1:M1),
    is_jam(J2:M2),
    T1 is J1 * 60 + M1,
    T2 is J2 * 60 + M2,
    Total is T1 + T2,
    JA is Total//60 mod 24,
    MA is Total mod 60.

% Rule untuk mengecek apakah batas waktu kurang dari 24 jam
is_time(24:00) :- !.
is_time(J:M) :- 
    J < 24, 
    J >= 0, 
    M =< 59, 
    M >= 0.

% Rule untuk mendefinisikan lama harus antara 1 sampai 24 jam
is_valid_lama(24:00) :- !.
is_valid_lama(J:M) :-
    J >= 0, 
    J =< 23, 
    M >= 0, 
    M =< 59.

% Rule untuk menambahkan hasil pengurangan jam (dimana J1:M1 - J2:M2 tidak boleh < 00:00)
subs_jam(J1:M1, J2:M2, JA:MA) :-
    is_time(J1:M1),
    is_time(J2:M2),
    T1 is J1 * 60 + M1,
    T2 is J2 * 60 + M2,
    Diff is T1-T2,
    Diff >= 0,
    JA is Diff//60,
    MA is Diff mod 60.

% Rule untuk membandingkan apakah tempat wisata sudah buka
is_buka(JB:MB, JD:MD) :-
    is_jam(JB:MB),
    is_jam(JD:MD), 
    JB < JD,
    !.

is_buka(JB:MB, JD:MD):-
    is_jam(JB:MB),
    is_jam(JD:MD), 
    JB =< JD,
    MB =< JD.

% Rule untuk membandingkan apakah tempat wisata belum tutup
isnot_tutup(JT:MT, JD:MD) :-
    is_jam(JT:MT),
    is_jam(JD:MD), 
    JD < JT,
    !.

isnot_tutup(JT:MT, JD:MD) :-
    is_jam(JT:MT),
    is_jam(JD:MD), 
    JD =< JT,
    MD =< JT.

% Rule-rule yang digunakan untuk memproses query
jadwal(Wisata, Lama, Mulai) :- 
    is_valid_lama(Lama),
    wisata(Tempat,Jam),
    subs_jam(Lama, Jam, WaktuBaru),
    add_jam(Mulai, Jam, MulaiBaru),
    jam_buka(Tempat, Buka, Tutup),
    is_buka(Buka, Mulai),
    is_buka(Buka, MulaiBaru),
    isnot_tutup(Tutup, MulaiBaru),
    jadwal_helper(WisataSelesai, Tempat, [mulai_pukul(Mulai), tiba(Tempat), pukul(Mulai),  berwisata(Tempat,Jam)], WaktuBaru, MulaiBaru, Selesai),
    append(WisataSelesai, [selesai_pukul(Selesai)], Wisata).

% Base Case (bila sisa waktu telah habis maka rute tersebut berhasil)
jadwal_helper(Wisata, _, Wisata, 00:M, Selesai, Selesai) :- M >= 0, M =< 15, !.

% Rule untuk menambahkan informasi perjalanan dari suatu tempat ke tempat lain
jadwal_helper(Wisata, TempatTerakhir, LW, Lama, Mulai, Selesai) :- 
    is_time(Lama), 
    perjalanan(TempatTerakhir, Tujuan, WaktuTempuh),
    subs_jam(Lama, WaktuTempuh, SisaWaktu),
    add_jam(Mulai, WaktuTempuh, MulaiBaru),
    \+member(perjalanan(TempatTerakhir, Tujuan, WaktuTempuh), LW),
    append(LW, [perjalanan(TempatTerakhir, Tujuan, WaktuTempuh)], LW2), 
    jadwal_helper2(Wisata, Tujuan, LW2, SisaWaktu, MulaiBaru, Selesai).

% Rule untuk menambahkan informasi mengenai tempat wisata serta lama waktu berwisata  
jadwal_helper2(Wisata, Tujuan, LW, Lama, Mulai, Selesai) :- 
    is_time(Lama), 
    wisata(Tujuan,Y),
    subs_jam(Lama,Y,Z),
    add_jam(Mulai,Y,M), 
    jam_buka(Tujuan, Buka, Tutup),
    is_buka(Buka, Mulai),
    is_buka(Buka, M),
    isnot_tutup(Tutup, M),
    \+member(tiba(Tujuan),LW), 
    append(LW, [tiba(Tujuan), pukul(Mulai) , berwisata(Tujuan,Y)], NL), 
    jadwal_helper(Wisata, Tujuan, NL, Z, M, Selesai).